Feature: User Management
  User Story (optional)
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non-admin users have no permissions over other accounts
  - There are two types of non-admin accounts:
  --Risk managers
  --Asset managers
  -non-admin users must be able to see risks or assets in their groups
  -The administrator must be able to reset users passwords
  -When an account is created, a user must be forced to reset their password on first logon

  Questions
  -Do admin users need access to all accounts or only to the accounts in their group?

  To do:
  -Force reset of password on first account creation

  Domain language:
  Group = Users are defined by which group they belong to
  CRUD = Create, REad, Update, Delete
  administrators permissions = access to CRUD risks, users and assets for all groups
  risk_management permissions = only able to CRUD risks
  asset_management permissions = only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@feB3ar
        And a user called tom with risk_management permissions and password S@feB3ar

  @high-impact
  Scenario Outline: The administrator checks a users details
    When simon is logged in with password S@feB3ar
    Then he is able to view <user>'s account
    Examples:
      | user |
      |tom   |

  @high-risk
  @to-do
  Scenario Outline: A user's password is reset by the administrator
    Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then simon can reset the <user>'s password
    Examples:
      | user |
      |tom   |

